import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubRouteMasterComponent } from './club-route-master.component';

describe('ClubRouteMasterComponent', () => {
  let component: ClubRouteMasterComponent;
  let fixture: ComponentFixture<ClubRouteMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClubRouteMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubRouteMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
