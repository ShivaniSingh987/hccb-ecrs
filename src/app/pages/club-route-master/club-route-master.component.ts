import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ClubRouteBUlistserviceService } from 'src/app/services/club-route-bulistservice.service';

@Component({
  selector: 'app-club-route-master',
  templateUrl: './club-route-master.component.html',
  styleUrls: ['./club-route-master.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class ClubRouteMasterComponent implements OnInit {
  b_list: any;
  bu_list = [];

  s_list: any;
  site_list = [];

  sit_list:any;
  site_listt = [];

  selectedBu: string='';
  selectedSite: any;

  // items = this.cartService.getItems();

  checkoutForm = this.formBuilder.group({
    name: '',
    address: ''
  });


  contactForm = new FormGroup({
    club_route_id: new FormControl(),
    club_route_desc: new FormControl(),
  })
  constructor(
    private BUlist: ClubRouteBUlistserviceService,
    config: NgbModalConfig, private modalService: NgbModal,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.BUlist.getbulist().subscribe(resp => {
      this.b_list = resp;
      this.bu_list = this.b_list.entity;
      console.log(this.bu_list)
      console.log("bulist",resp)
    })
  }

modo(){
  console.log("Hi")
  this.BUlist.getsitelist(this.selectedBu).subscribe(resp => {
    this.s_list = resp;
    this.site_list = this.s_list.entity;
    console.log(resp)
  })
}

showCRM(){
  console.log("CRM")
  this.BUlist.fetchCRMId(this.selectedBu, this.selectedSite).subscribe(resp => {
    this.sit_list = resp;
    this.site_listt = this.sit_list.entity;

    console.log(this.site_list)
  })
}

open(content:any) {
  this.modalService.open(content);

  this.showCRM()
}

onSubmit(): void {
  // Process checkout data here
  // this.items = this.cartService.clearCart();
  console.warn('Your order has been submitted', this.checkoutForm.value);
  this.checkoutForm.reset();
}
  

}
