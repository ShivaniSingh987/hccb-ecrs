import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClubRouteMasterComponent } from './club-route-master/club-route-master.component';

const routes: Routes = [
  { path: '', redirectTo: 'ClubRouteMaster', pathMatch: 'full' },
  { path: 'ClubRouteMaster', component: ClubRouteMasterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
