import { TestBed } from '@angular/core/testing';

import { ClubRouteBUlistserviceService } from './club-route-bulistservice.service';

describe('ClubRouteBUlistserviceService', () => {
  let service: ClubRouteBUlistserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClubRouteBUlistserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
