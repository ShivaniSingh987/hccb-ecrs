import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ClubRouteBUlistserviceService {

  apiURL = 'https://localhost:5001/api/ClubRoute'
  constructor(public http: HttpClient) { }


  getbulist() {
    return this.http.post<any>(this.apiURL + '/fetchDropDownData', { "userID": "1" })
  }

  getsitelist(eId: string) {
    return this.http.post<any>(this.apiURL + '/fetchSiteDropDownData', { "admin_Entity_Id": eId })
  }

  fetchCRMId(eId: string,  siteId: string) {
    return this.http.post<any>(this.apiURL + '/fetchCRMId', {
      "admin_Entity_Id": eId,
      "site_id": siteId,
      "crmid": "",
      "crMdesc": "",
      "isactive": "",
      "createdBy": "",
      "operation": "Id"
    })
  }
}
